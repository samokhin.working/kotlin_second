package homework2

import org.junit.Assert.assertEquals
import org.junit.Assert.assertThrows
import org.junit.Rule
import org.junit.Test
import org.junit.contrib.java.lang.system.SystemOutRule


class CalculatorTest {

    @get:Rule
    val systemOutRule = SystemOutRule().enableLog()!!

    @Test
    fun testCalculatorAddition() {
        calculator(sum, 2.0, 3.0)
        assertEquals("5.0\n", systemOutRule.log)
    }

    @Test
    fun testCalculatorSubtraction() {
        calculator(subtraction, 10.0, 13.0)
        assertEquals("-3.0\n", systemOutRule.log)
    }

    @Test
    fun testCalculatorMultiplication() {
        calculator(product, 11.0, 3.4)
        assertEquals("37.4\n", systemOutRule.log)
    }

    @Test
    fun testCalculatorDivision() {
        calculator(division, 36.0, 2.5)
        assertEquals("14.4\n", systemOutRule.log)
    }

    @Test
    fun testCalculatorDivisionByZero() {
        assertThrows(ArithmeticException::class.java) { calculator(division, 17.0, 0.0) }
    }
}
