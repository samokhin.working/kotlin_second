import homework5.Person
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import kotlin.random.Random
import java.time.temporal.ChronoUnit

class GenerationPersonSystem() {
    fun generateName(gender: String): String {
        val maleNames = listOf(
            "Александр",
            "Алексей",
            "Андрей",
            "Антон",
            "Артём",
            "Борис",
            "Вадим",
            "Валентин",
            "Валерий",
            "Василий",
            "Виктор",
            "Виталий",
            "Владимир",
            "Геннадий",
            "Георгий",
            "Даниил",
            "Денис",
            "Дмитрий",
            "Евгений",
            "Иван",
            "Игорь",
            "Константин",
            "Леонид",
            "Максим",
            "Михаил",
            "Никита",
            "Николай",
            "Олег",
            "Павел",
            "Роман",
            "Руслан",
            "Сергей",
            "Станислав",
            "Тимофей",
            "Фёдор",
            "Юрий",
            "Ян",
            "Ярослав"
        )
        val femaleNames = listOf(
            "Александра",
            "Алина",
            "Алиса",
            "Анастасия",
            "Ангелина",
            "Анна",
            "Арина",
            "Валентина",
            "Валерия",
            "Вера",
            "Вероника",
            "Виктория",
            "Галина",
            "Дарья",
            "Евгения",
            "Екатерина",
            "Елена",
            "Зоя",
            "Инна",
            "Ирина",
            "Карина",
            "Кристина",
            "Лариса",
            "Лидия",
            "Лилия",
            "Любовь",
            "Маргарита",
            "Марина",
            "Надежда",
            "Наталья",
            "Оксана",
            "Олеся",
            "Ольга",
            "Полина",
            "Светлана",
            "София",
            "Тамара",
            "Татьяна",
            "Ульяна",
            "Юлия"
        )
        return if (gender == "м") maleNames.random() else femaleNames.random()
    }

    fun generateSurname(gender: String): String {
        val maleSurnames = listOf(
            "Иванов",
            "Петров",
            "Сидоров",
            "Смирнов",
            "Кузнецов",
            "Соколов",
            "Михайлов",
            "Федоров",
            "Морозов",
            "Волков",
            "Алексеев",
            "Лебедев",
            "Семенов",
            "Егоров",
            "Павлов",
            "Козлов",
            "Степанов",
            "Николаев",
            "Орлов",
            "Андреев",
            "Макаров",
            "Новиков",
            "Медведев",
            "Яковлев",
            "Новиков",
            "Романов",
            "Васильев",
            "Зайцев",
            "Поляков",
            "Чернов",
            "Белов",
            "Крылов",
            "Калашников",
            "Колесников",
            "Маслов",
            "Исаев",
            "Тихонов",
            "Анисимов",
            "Ефимов",
            "Куликов",
            "Шубин"
        )
        val femaleSurnames = listOf(
            "Иванова",
            "Петрова",
            "Сидорова",
            "Смирнова",
            "Кузнецова",
            "Соколова",
            "Михайлова",
            "Федорова",
            "Морозова",
            "Волкова",
            "Алексеева",
            "Лебедева",
            "Семенова",
            "Егорова",
            "Павлова",
            "Козлова",
            "Степанова",
            "Николаева",
            "Орлова",
            "Андреева",
            "Макарова",
            "Новикова",
            "Медведева",
            "Яковлева",
            "Новикова",
            "Романова",
            "Васильева",
            "Зайцева",
            "Полякова",
            "Чернова",
            "Белова",
            "Крылова",
            "Калашникова",
            "Колесникова",
            "Маслова",
            "Исаева",
            "Тихонова",
            "Анисимова",
            "Ефимова",
            "Куликова",
            "Шубина"
        )
        return if (gender == "м") maleSurnames.random() else femaleSurnames.random()
    }

    fun generatePatronymic(gender: String): String {
        val malePatronymics = listOf(
            "Александрович",
            "Андреевич",
            "Борисович",
            "Вадимович",
            "Валентинович",
            "Васильевич",
            "Викторович",
            "Геннадьевич",
            "Георгиевич",
            "Дмитриевич",
            "Евгеньевич",
            "Игоревич",
            "Ильич",
            "Константинович",
            "Леонидович",
            "Максимович",
            "Михайлович",
            "Никитич",
            "Николаевич",
            "Олегович",
            "Павлович",
            "Романович",
            "Семенович",
            "Сергеевич",
            "Станиславович",
            "Степанович",
            "Тимофеевич",
            "Федорович",
            "Юрьевич",
            "Ярославович",
            "Артурович",
            "Арсеньевич",
            "Вячеславович",
            "Валерьевич",
            "Владимирович",
            "Германович",
            "Григорьевич",
            "Данилович",
            "Егорович",
            "Кириллович",
            "Матвеевич",
            "Маркович"
        )
        val femalePatronymics = listOf(
            "Александровна",
            "Андреевна",
            "Антоновна",
            "Борисовна",
            "Вадимовна",
            "Валерьевна",
            "Васильевна",
            "Викторовна",
            "Владимировна",
            "Геннадьевна",
            "Георгиевна",
            "Дмитриевна",
            "Евгеньевна",
            "Ивановна",
            "Игоревна",
            "Константиновна",
            "Леонидовна",
            "Максимовна",
            "Михайловна",
            "Николаевна",
            "Олеговна",
            "Павловна",
            "Петровна",
            "Романовна",
            "Семеновна",
            "Сергеевна",
            "Станиславовна",
            "Степановна",
            "Федоровна",
            "Юрьевна",
            "Ярославовна",
            "Алексеевна",
            "Аркадьевна",
            "Григорьевна",
            "Денисовна",
            "Ефимовна",
            "Кирилловна",
            "Марковна",
            "Назаровна",
            "Филипповна",
            "Эдуардовна"
        )
        return if (gender == "м") malePatronymics.random() else femalePatronymics.random()
    }

    fun generateGender(): String {
        return if (Random.nextBoolean()) "м" else "ж"
    }

    fun generateBirthDate(): String {
        val formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy")
        val startYear = 1950
        val endYear = LocalDate.now().year - 18 // Ограничиваем возраст до 18 лет назад
        val startMonth = 1
        val endMonth = 12
        val startDay = 1
        val endDay = 28 // Предполагаем, что все месяцы имеют 28 дней

        val year = Random.nextInt(startYear, endYear + 1)
        val month = Random.nextInt(startMonth, endMonth + 1)
        val day = Random.nextInt(startDay, endDay + 1)

        val date = LocalDate.of(year, month, day)
        return date.format(formatter)
    }

    fun generateBirthPlace(): String {
        val cities = listOf(
            "Москва",
            "Санкт-Петербург",
            "Новосибирск",
            "Екатеринбург",
            "Нижний Новгород",
            "Казань",
            "Самара",
            "Омск",
            "Челябинск",
            "Ростов-на-Дону",
            "Уфа",
            "Волгоград",
            "Пермь",
            "Красноярск",
            "Воронеж",
            "Саратов",
            "Краснодар",
            "Тольятти",
            "Ижевск",
            "Барнаул",
            "Ульяновск",
            "Владивосток",
            "Ярославль",
            "Иркутск",
            "Тюмень",
            "Махачкала",
            "Хабаровск",
            "Оренбург",
            "Новокузнецк",
            "Кемерово",
            "Рязань",
            "Томск",
            "Астрахань",
            "Пенза",
            "Липецк",
            "Тула",
            "Киров",
            "Чебоксары",
            "Курск",
            "Магнитогорск",
            "Набережные Челны",
            "Симферополь"
        )
        return cities.random()
    }

    fun calculateAge(dateOfBirth: String, currentDate: String): Int {
        val formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy")
        val birthDate = LocalDate.parse(dateOfBirth, formatter)
        val currentDateFormatted = LocalDate.parse(currentDate, formatter)
        return ChronoUnit.YEARS.between(birthDate, currentDateFormatted).toInt()
    }

    fun generatePerson(currentDate: String): Person {
        val gender = generateGender()
        val dateOfBirth = generateBirthDate()
        return Person(
            gender = gender,
            name = generateName(gender),
            surname = generateSurname(gender),
            patronymic = generatePatronymic(gender),
            age = calculateAge(dateOfBirth, currentDate),
            dateOfBirth = dateOfBirth,
            birthPlace = generateBirthPlace()
        )
    }

    fun generatePersons(currentDate: String, n: Int): List<Person> {
        val persons = mutableListOf<Person>()

        repeat(n) {
            val person = generatePerson(currentDate)
            persons.add(person)
        }
        return persons
    }

}
