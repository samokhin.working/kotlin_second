package homework5

import GenerationPersonSystem
import com.google.gson.Gson
import com.itextpdf.kernel.font.PdfFont
import com.itextpdf.kernel.pdf.PdfDocument
import com.itextpdf.kernel.font.PdfFontFactory
import com.itextpdf.kernel.pdf.PdfWriter
import com.itextpdf.layout.Document
import com.itextpdf.layout.element.Cell
import com.itextpdf.layout.element.Table
import com.itextpdf.layout.element.Paragraph
import com.itextpdf.layout.property.TextAlignment
import java.io.File
import java.util.Scanner

data class Person(
    val name: String,
    val surname: String,
    val patronymic: String,
    val gender: String,
    val age: Int,
    val dateOfBirth: String,
    val birthPlace: String
)

data class Address(
    val postalCode: String,
    val country: String,
    val region: String,
    val city: String,
    val street: String,
    val house: String,
    val apartment: String
)

data class PersonWithAddress(
    val person: Person,
    val address: Address
)

fun main() {

    val currentDate = "17-05-2023"

    val scanner = Scanner(System.`in`)
    println("Введите число n:")
    val n = scanner.nextInt()
    val systemPerson = GenerationPersonSystem()
    val systemAddress = GenerationAddressSystem()
    val persons = systemPerson.generatePersons(currentDate, n)
    val addresses = systemAddress.generateAddress(n)

    val personWithAddress = Array(persons.size) { i ->
        PersonWithAddress(persons[i], addresses[i])
    }

    val gson = Gson()
    val json = gson.toJson(personWithAddress)
    val file2 = File("persons.json")
    file2.writeText(json)
    println("JSON-файл 'persons.json' успешно создан. Путь: ~/IdeaProjects/kotlin_second - корень проекта")

    val pdfFile = "persons.pdf"
    val file = File(pdfFile)
    val writer = PdfWriter(file)
    val pdf = PdfDocument(writer)

    val document = Document(pdf)
    val table = Table(14)

    table.addHeaderCell(createCell("Имя", getDefaultFont()))
    table.addHeaderCell(createCell("Фамилия", getDefaultFont()))
    table.addHeaderCell(createCell("Отчество", getDefaultFont()))
    table.addHeaderCell(createCell("Пол", getDefaultFont()))
    table.addHeaderCell(createCell("Возраст", getDefaultFont()))
    table.addHeaderCell(createCell("Дата рождения", getDefaultFont()))
    table.addHeaderCell(createCell("Место рождения", getDefaultFont()))
    table.addHeaderCell(createCell("Индекс", getDefaultFont()))
    table.addHeaderCell(createCell("Страна", getDefaultFont()))
    table.addHeaderCell(createCell("Область", getDefaultFont()))
    table.addHeaderCell(createCell("Город", getDefaultFont()))
    table.addHeaderCell(createCell("Улица", getDefaultFont()))
    table.addHeaderCell(createCell("Дом", getDefaultFont()))
    table.addHeaderCell(createCell("Квартира", getDefaultFont()))

    for (person in personWithAddress) {
        table.addCell(createCell(person.person.name, getDefaultFont()))
        table.addCell(createCell(person.person.surname, getDefaultFont()))
        table.addCell(createCell(person.person.patronymic, getDefaultFont()))
        table.addCell(createCell(person.person.gender, getDefaultFont()))
        table.addCell(createCell(person.person.age.toString(), getDefaultFont()))
        table.addCell(createCell(person.person.dateOfBirth, getDefaultFont()))
        table.addCell(createCell(person.person.birthPlace, getDefaultFont()))
        table.addCell(createCell(person.address.postalCode, getDefaultFont()))
        table.addCell(createCell(person.address.country, getDefaultFont()))
        table.addCell(createCell(person.address.region, getDefaultFont()))
        table.addCell(createCell(person.address.city, getDefaultFont()))
        table.addCell(createCell(person.address.street, getDefaultFont()))
        table.addCell(createCell(person.address.house, getDefaultFont()))
        table.addCell(createCell(person.address.apartment, getDefaultFont()))
    }
    document.add(table)
    document.close()

    println("PDF-файл '$pdfFile' успешно создан. Путь: ~/IdeaProjects/kotlin_second - корень проекта")
}

private fun getDefaultFont(): PdfFont {
    return PdfFontFactory.createFont(
        "/Users/v.samokhin/IdeaProjects/kotlin_second/src/main/resources/assets/fonts/arial.ttf",
        "Identity-H",
        true
    )
}

private fun createCell(text: String, font: PdfFont): Cell {
    val paragraph = Paragraph(text).setFont(font).setFontSize(8F)
    val cell = Cell().add(paragraph)
    cell.setTextAlignment(TextAlignment.CENTER)
    return cell
}
