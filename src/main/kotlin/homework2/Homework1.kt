package homework2

import kotlin.math.roundToInt

fun transformCollection(list: List<Double?>) {
    val result = list
        .asSequence()
        .mapNotNull {
            it
        }
        .map {
            if (it.toInt() % 2 == 0) it * it else it / 2.0
        }
        .filterNot {
            it > 25.0
        }
        .sortedDescending()
        .take(10)
        .reduce { sum, item -> sum + item }

    println((result * 100).roundToInt() / 100.0)

}

fun main() {

    val list1 = listOf(13.31, 3.98, 12.0, 2.99, 9.0)
    val list2 = listOf(133.21, null, 233.98, null, 26.99, 5.0, 7.0, 9.0)

    transformCollection(list1)
    transformCollection(list2)
}