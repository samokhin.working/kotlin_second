package homework2

abstract class Pet(var name: String)

interface Runnable {
    fun run()
}

interface Swimmable {
    fun swim()
}

open class Cat(name: String) : Pet(name), Runnable, Swimmable {
    override fun run() {
        println("I am a Cat and i running")
    }

    override fun swim() {
        println("I am a Cat and i swimming")
    }
}

open class Fish(name: String) : Pet(name), Swimmable {
    override fun swim() {
        println("I am a Fish and i swimming")
    }
}

class Tiger(name: String) : Cat(name) {
    override fun run() {
        println("I am a Tiger and i running")
    }

    override fun swim() {
        println("I am a Tiger and i swimming")
    }
}

class Lion(name: String) : Cat(name) {
    override fun run() {
        println("I am a Lion and i running")
    }

    override fun swim() {
        println("I am a Lion and i swimming")
    }
}

class Salmon(name: String) : Fish(name) {
    override fun swim() {
        println("I am a Salmon and i swimming")
    }
}

class Tuna(name: String) : Fish(name) {
    override fun swim() {
        println("I am a Tuna and i swimming")
    }
}


fun <T : Runnable> useRunSkill(pet: T) {
    pet.run()
}

fun <T : Swimmable> useSwimSkill(pet: T) {
    pet.swim()
}

fun <T> useSwimAndRunSkill(pet: T)
        where T : Runnable,
              T : Swimmable {
    pet.run()
    pet.swim()
}


fun main() {

    val tiger = Tiger("Barsik")
    val lion = Lion("Simba")
    val salmon = Salmon("Nemo")
    val tuna = Tuna("Tommy")

    val cats: List<Cat> = listOf(tiger, lion)
    val fish: List<Fish> = listOf(salmon, tuna)

    cats.forEach {
        useRunSkill(it)
        useSwimSkill(it)
        useSwimAndRunSkill(it)
        println()
    }

    fish.forEach {
        useSwimSkill(it)
        println()
    }

}