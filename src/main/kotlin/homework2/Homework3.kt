package homework2

val sum = { a: Double, b: Double -> a + b }
val subtraction = { a: Double, b: Double -> a - b }
val product = { a: Double, b: Double -> a * b }
val division = { a: Double, b: Double ->
    if (!b.equals(0.0)) a / b
    else throw ArithmeticException("cannot be divided by zero")
}

fun main() {
    calculator(getCalculationMethod("+"), 2.0, 3.0)
    calculator(getCalculationMethod("-"), 2.0, 13.0)
    calculator(getCalculationMethod("*"), 11.0, 3.4)
    calculator(getCalculationMethod("/"), 47.0, 2.5)
    calculator(getCalculationMethod("/"), 47.0, 0.0)

}

fun calculator(lambda: ((Double, Double) -> Double), a: Double, b: Double) {
    println(lambda(a, b))
}

fun getCalculationMethod(name: String): (Double, Double) -> Double {
    return when (name) {
        "+" -> sum
        "-" -> subtraction
        "*" -> product
        "/" -> division
        else -> throw UnsupportedOperationException()
    }
}