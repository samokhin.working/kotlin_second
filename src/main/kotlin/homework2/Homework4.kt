package homework2


fun validateInnFunction(inn: String, function: (String) -> Boolean) {
    if (inn.length < 12) return println("Производится проверка только для 12-ти значного ИНН")
    if (!inn.matches("[0-9]{12}".toRegex())) {
        return println("Некорректный ИНН. В ИНН используются только цифры от 0 до 9")
    }
    println("ИНН $inn ${if (function(inn)) "валиден" else "не валиден"}")
}


val validInn = { inn: String ->
    inn[10].digitToInt() == createNumber(inn, true)
            && inn[11].digitToInt() == createNumber(inn, false)
}

val firtsCoeff = listOf(7, 2, 4, 10, 3, 5, 9, 4, 6, 8)
val secondCoeff = listOf(3, 7, 2, 4, 10, 3, 5, 9, 4, 6, 8)

fun createNumber(inn: String, isFirst: Boolean): Int {
    val digitArray = inn.toCharArray().map { it.digitToInt() }
    var sum = 0

    if (isFirst) {
        for (i in 0 until 10) {
            sum += digitArray[i] * firtsCoeff[i]
        }
        sum %= 11
    } else {
        for (i in 0 until 11) {
            sum += digitArray[i] * secondCoeff[i]
        }
        sum %= 11
    }

    return sum
}


fun main() {
    val inn = "707574944998"
    val inn2 = "707574944991"
    val inn3 = "500100732259"
    val inn4 = "3664069397"
    val inn5 = "366406939ABC"

    validateInnFunction(inn, validInn)
    validateInnFunction(inn2, validInn)
    validateInnFunction(inn3, validInn)
    validateInnFunction(inn4, validInn)
    validateInnFunction(inn5, validInn)

}
