package homework1

private fun personalInfo(
    name: String, surname: String, patronymic: String? = null, gender: String,
    age: Int, dateOfBirth: String, inn: String? = null, snils: String? = null
) {

    print("name = $name, surname = $surname, ")
    patronymic?.let {
        print("patronymic = $it, ")
    }
    print(
        "gender = $gender, age = $age, " +
                "dateOfBirth = $dateOfBirth"
    )
    inn?.let {
        print(", inn = $it")
    }
    snils?.let {
        print(", snils = $it")
    }
    println()
}

fun main() {
    personalInfo(name = "Van", surname = "Darkholml", gender = "Male", age = 43, dateOfBirth = "1980-01-04")
    personalInfo(
        name = "Igor", surname = "Nikolaev", patronymic = "Vladimirovich",
        gender = "FiveReasonMan", age = 63, dateOfBirth = "1960-01-30", inn = "1234567890", snils = "111-111-111"
    )
    personalInfo(
        gender = "Male", name = "Vasiliy", dateOfBirth = "1981-02-12", inn = "1246786420", surname = "Petrovskiy",
        age = 42, snils = "222-222-222"
    )

}
