package homework1

fun testStringFunction(vararg params: String) {
    println("Передано элементов ${params.size}:")
    params.forEach {
        print("$it; ")
    }
    println()
    println()
}

fun main() {
    testStringFunction("12", "122", "1234", "fpo")
    testStringFunction("ajk", "23")
    testStringFunction("one")
}