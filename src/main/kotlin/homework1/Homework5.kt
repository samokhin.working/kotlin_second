package homework1

class Homework5 {
    companion object {
        private var myCounter = 0
        fun counter() {
            myCounter++
            println("Вызыван counter. Количество вызовов = $myCounter")
        }
    }
}

fun main() {
    Homework5.counter()
    Homework5.counter()
    Homework5.counter()
}
