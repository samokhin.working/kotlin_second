package homework1

import java.time.LocalDate
import java.util.logging.Handler
import kotlin.math.roundToInt

fun typeCasting(anything: Any?) {
    val dateOfTinkoff = LocalDate.of(2006, 12, 24)
    when (anything) {
        null -> println("Объект равен null")
        is String -> println("Я получил String = $anything, ее длина равна ${anything.length}")
        is Int -> println("Я получил Int = $anything, его квадрат равен ${anything * anything}")
        is Double -> println(
            "Я получил Double = $anything, это число округляется до ${
                if ((anything * 100).roundToInt() % 100 != 0) (anything * 100).roundToInt().toDouble() / 100.0
                else
                    (anything * 100).roundToInt() / 100
            }"
        )
        is LocalDate -> println(
            "Я получил LocalDate $anything, ${
                if (anything < dateOfTinkoff)
                    "эта дата меньше чем дата основания Tinkoff" else "эта дата позже чем дата основания Tinkoff"
            }"
        )
        else -> println("Мне этот тип неизвестен(")

    }
}

fun main() {
    typeCasting("Privet")
    typeCasting(145)
    typeCasting(145.0)
    typeCasting(145.2817812)
    typeCasting(2.356)
    typeCasting(LocalDate.of(1990, 1, 1))
    typeCasting(LocalDate.of(2023, 3, 16))
    typeCasting(null)
    typeCasting(Handler::class)
}