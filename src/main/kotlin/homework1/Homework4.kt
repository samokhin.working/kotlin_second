package homework1

data class Person(
    val name: String, val surname: String, val patronymic: String?, val gender: String,
    val age: Int, val dateOfBirth: String, val inn: String?, val snils: String?
) {
}

private fun personalInfo(
    name: String, surname: String, patronymic: String? = null, gender: String,
    age: Int, dateOfBirth: String, inn: String? = null, snils: String? = null
): Person {
    return Person(
        name = name,
        surname = surname,
        patronymic = patronymic,
        gender = gender,
        age = age,
        dateOfBirth = dateOfBirth,
        inn = inn,
        snils = snils
    )
}

fun main() {
    println(
        personalInfo(
            name = "Van", surname = "Darkholml", gender = "Male",
            age = 43, dateOfBirth = "1980-01-04"
        ).toString()
    )
    println(
        personalInfo(
            name = "Igor", surname = "Nikolaev", patronymic = "Vladimirovich",
            gender = "FiveReasonMan", age = 63, dateOfBirth = "1960-01-30", inn = "1234567890", snils = "111-111-111"
        ).toString()
    )
    println(
        personalInfo(
            gender = "Male", name = "Vasiliy", dateOfBirth = "1981-02-12", inn = "1246786420", surname = "Petrovskiy",
            age = 42, snils = "222-222-222"
        ).toString()
    )
}
