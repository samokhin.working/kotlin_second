package homework1

fun MutableList<Int>.squareValues() {
    for (i in this.indices) {
        this[i] = this[i] * this[i]
    }
}

fun main() {
    val list = mutableListOf(1, 4, 9, 16, 25)
    list.squareValues()
    println(list)
}
